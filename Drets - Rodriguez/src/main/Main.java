
package main;

import dominio.Sistema;
import interfaz.Principal;


public class Main {
    public static void main(String[] args) {
        Sistema sistema = new Sistema();
        Principal ventana = new Principal(sistema);
        ventana.setVisible(true);
    }
}
