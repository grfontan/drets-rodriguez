
package dominio;

import java.util.ArrayList;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/*import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

*/
public class Sorteo {
    private ArrayList<Usuario> usuario;
    private Restaurant restaurant;
    private String fecha;
    private int numeroGanadores;
    private String descripcion;
    private ArrayList<Usuario> ganadores;

    public ArrayList<Usuario> getUsuario() {
        return usuario;
    }

    public Sorteo() {
        usuario = new ArrayList<>();
        ganadores = new ArrayList<>();
    }

    public ArrayList<Usuario> getGanadores() {
        return ganadores;
    }

    public void setGanadores(ArrayList<Usuario> ganadores) {
        this.ganadores = ganadores;
    }

    public void setUsuario(ArrayList<Usuario> usuario) {
        this.usuario = usuario;
    }

    

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getNumeroGanadores() {
        return numeroGanadores;
    }

    public void setNumeroGanadores(int numeroGanadores) {
        this.numeroGanadores = numeroGanadores;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public void realizarSorteo() {
        if (this.getNumeroGanadores() <= this.getUsuario().size()){
            for(int i = 0; i < this.numeroGanadores; i++){
                int random = (int) (Math.random()*this.getUsuario().size()+1);
                Usuario ganador = this.getUsuario().get(random-1);
                this.getGanadores().add(ganador);

                //Para que no pueda ganar dos veces el mismo usuario
                int posicionEnOtraLista = this.getUsuario().indexOf(ganador);
                this.getUsuario().remove(posicionEnOtraLista);
            }

            for(int i = 0; i < this.getGanadores().size(); i++){
                Usuario ganador = this.getGanadores().get(i);
                this.getUsuario().add(ganador);

            }
            mandarMailGanadores();
        }
    }
    
    
    public void mandarMailGanadores(){
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
 
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("sorteorestaurant@gmail.com", "casanovista");
                    }
                });
 
        for (int i = 0; i < this.getGanadores().size(); i++){
            Usuario usuario = this.getGanadores().get(i);
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("sorteorestaurant@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(usuario.getMail()));
                message.setSubject("Usted es ganador del sorteo");
                message.setText("Felicitaciones! Usted ha ganado el premio numero " + (i+1) + "  del sorteo del restaurant" +
                                this.getRestaurant() + ". Descripcion del sorteo: " +this.getDescripcion() + 
                                ". Contactese con el restaurant para saber como recalamar su premio");

                Transport.send(message);
            }
            catch (Exception e) {
            }
        }
    }
    
    @Override
    public boolean equals(Object o){
      if (o == null){
          return false;
      }
      
      if (o.getClass() != this.getClass()){
          return false;
      }
      return this.getRestaurant().equals(((Sorteo)o).getRestaurant());
    }
    
    
    @Override
    public String toString(){
        return this.getRestaurant().toString();
    }
    
    
    
    
}
