
package dominio;

import java.util.ArrayList;


public class Sistema {
    private ArrayList<Restaurant> listaRestaurant;
    private ArrayList<Evaluacion> listaEvaluacion;
    private ArrayList<Usuario> listaUsuarios;
    private ArrayList<Sorteo> listaSorteo;

    public Sistema() {
        listaRestaurant = new ArrayList<>();
        listaEvaluacion = new ArrayList<>();
        listaUsuarios = new ArrayList<>();
        listaSorteo = new ArrayList<>();
    }

    public ArrayList<Restaurant> getListaRestaurant() {
        return listaRestaurant;
    }

    public void setListaRestaurant(ArrayList<Restaurant> listaRestaurant) {
        this.listaRestaurant = listaRestaurant;
    }

    public ArrayList<Evaluacion> getListaEvaluacion() {
        return listaEvaluacion;
    }

    public void setListaEvaluacion(ArrayList<Evaluacion> listaEvaluacion) {
        this.listaEvaluacion = listaEvaluacion;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public ArrayList<Sorteo> getListaSorteo() {
        return listaSorteo;
    }

    public void setListaSorteo(ArrayList<Sorteo> listaSorteo) {
        this.listaSorteo = listaSorteo;
    }

    public void usuariosParticipantes(Sorteo elSorteo, Restaurant elRestaurant){
        ArrayList<Usuario> participantes = new ArrayList<>();
        
        for(int i = 0; i < this.getListaEvaluacion().size(); i++){
            Evaluacion laEvaluacion = this.getListaEvaluacion().get(i);
            if(!laEvaluacion.isEsAnonima() && laEvaluacion.getRestaurant().equals(elRestaurant)){
                participantes.add(laEvaluacion.getUsuario());
            }
        }
        
        elSorteo.setUsuario(participantes);
    }
    
    public ArrayList<Evaluacion> evaluacionPorRestaurant(Restaurant elRestaurant){
         ArrayList<Evaluacion> lista = new ArrayList<>();
         for(int i = 0; i < this.getListaEvaluacion().size(); i++){
             Evaluacion laEval = this.getListaEvaluacion().get(i);
             if (laEval.getRestaurant().equals(elRestaurant)){
                 lista.add(laEval);
             }
         }
        
        return lista;
    }

    
    
}
