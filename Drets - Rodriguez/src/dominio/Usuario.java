
package dominio;


public class Usuario {
    private String nombre;
    private String mail;

    public Usuario(String nombre, String mail) {
        this.nombre = nombre;
        this.mail = mail;
    }

    public Usuario() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public boolean esMailValido(String elMail){
        
         //Variables del metedo
        int cont = 0;
        String caracter;
        int largo = elMail.length();
        
        //Recorrer todo el String, y contar la cantidad de arrobas
        for (int i = 0; i < largo; i++) {
            caracter = String.valueOf(elMail.charAt(i));
            if (caracter.equals("@")){
                cont++;
            }
            
        }
        
        //Retorno si se conto un y solo un arroba
        return (cont==1);
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
          return false;
        }
      
        if (o.getClass() != this.getClass()){
              return false;
         }
        return this.getMail().equals(((Usuario)o).getMail());
    }
    
    @Override
    public String toString(){
        return this.getMail();
    }
    
    
}
