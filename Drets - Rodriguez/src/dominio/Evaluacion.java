
package dominio;

public class Evaluacion {
    private int estrellas;
    private String reseña;
    private Usuario usuario;
    private boolean esAnonima;
    private Restaurant restaurant;

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Evaluacion(int estrellas, String reseña, Usuario usuario, boolean esAnonima) {
        this.estrellas = estrellas;
        this.reseña = reseña;
        this.usuario = usuario;
        this.esAnonima = esAnonima;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    public String getReseña() {
        return reseña;
    }

    public Evaluacion() {
        
    }

    public void setReseña(String reseña) {
        this.reseña = reseña;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean isEsAnonima() {
        return esAnonima;
    }

    public void setEsAnonima(boolean esAnonima) {
        this.esAnonima = esAnonima;
    }
    
    public boolean estrellasValidas(int unaEstrella){
        return ((unaEstrella>=1) && (unaEstrella<=5));
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
          return false;
         }
      
        if (o.getClass() != this.getClass()){
            return false;
        }
        return ((this.getUsuario().equals(((Evaluacion)o).getUsuario())) &&(this.getReseña().equals((((Evaluacion)o).getReseña()))));
    }
    
    @Override
    public String toString(){
        if (this.isEsAnonima()){
            return "Anónima: " + this.getReseña();
        }
        else{
            return this.getUsuario() + ": " + this.getReseña();
        }
    }
}
