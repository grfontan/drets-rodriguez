
package dominio;

public class Restaurant {
    private String nombre;
    private String direccion;
    private String horario;
    private String tipoComida;
    private int puntacion;
    private int totalEstrellas;
    private int cantEvaluaciones;

    public Restaurant(String nombre) {
        this.nombre = nombre;
    }
    
    public int getTotalEstrellas() {
        return totalEstrellas;
    }

    public void setTotalEstrellas(int totalEstrellas) {
        this.totalEstrellas = this.totalEstrellas + totalEstrellas;
    }
    

    public Restaurant(String nombre, String direccion, String horario, String tipoComida) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.horario = horario;
        this.tipoComida = tipoComida;
    }

    public Restaurant() {
        puntacion = 0;
        totalEstrellas = 0;
        cantEvaluaciones = 0;
    }

    public int getPuntacion() {
        return puntacion;
    }

    public void setPuntacion() {
        if (cantEvaluaciones != 0)
            this.puntacion = totalEstrellas / cantEvaluaciones;
        else
            this.puntacion = 0;
    }

    public int getCantEvaluaciones() {
        return cantEvaluaciones;
    }

    public void setCantEvaluaciones() {
        this.cantEvaluaciones++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getTipoComida() {
        return tipoComida;
    }

    public void setTipoComida(String tipoComida) {
        this.tipoComida = tipoComida;
    }
    
    public void agregarPuntacion(int laPuntuacion){
        this.setCantEvaluaciones();
        this.setTotalEstrellas(laPuntuacion);
        this.setPuntacion();
        
    }
    
    public boolean esDatoVacio(String dato){
        return dato.trim().isEmpty();
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
          return false;
        }
      
        if (o.getClass() != this.getClass()){
          return false;
         }
        return this.getNombre().equals(((Restaurant)o).getNombre());
    }
    
    @Override
    public String toString(){
        return this.getNombre();
    }
 
}
