
package interfaz;

import dominio.Evaluacion;
import dominio.Restaurant;
import dominio.Sistema;
import dominio.Sorteo;
import dominio.Usuario;
import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class Principal extends javax.swing.JFrame {

    private Sistema modelo;
    private String horarios = "";
    private ImageIcon estrellaBlanca;
    private ImageIcon estrellaLLena;
    private ImageIcon btnRestBlanco;
    private ImageIcon btnRestRojo;
    private ImageIcon btnSorteoBlanco;
    private ImageIcon btnSorteoRojo;
    private ImageIcon btnHacerSorteoBlanco;
    private ImageIcon btnHacerSorteoRojo;
    private ImageIcon btnEvalBlanco;
    private ImageIcon btnEvalRojo;
    private ImageIcon btnInfoBlanco;
    private ImageIcon btnInfoRojo;
    private int cantEstrellas = 0;
    private boolean debeCambiar = false;
    private boolean ingresoFecha = false;
    private Restaurant restaurantAux;
    
    public Principal(Sistema unModelo) {
        initComponents();
        modelo = unModelo;
        
        this.setTitle("Juan Drets - Guillermo Rodriguez");
        
        URL img1 = Principal.class.getResource("/estrella blanca.png");
        URL img2 = Principal.class.getResource("/estrella.png");
        
        URL urlEvalBlanco = Principal.class.getResource("/eval blanco.png");
        URL urlEvalRojo = Principal.class.getResource("/eval rojo.png");
        
        URL urlRstBlanco = Principal.class.getResource("/boton restaurant blanco.png");
        URL urlRstRojo = Principal.class.getResource("/boton restaurant rojo.png");
        
        URL urlInfoBlanco = Principal.class.getResource("/boton informacion.png");
        URL urlInfoRojo = Principal.class.getResource("/boton informacion rojo.png");
        
        URL urlSorteoBlanco = Principal.class.getResource("/botones sor blanco.png");
        URL urlSorteoRojo = Principal.class.getResource("/botones sort rojo.png");
        
        URL urlHacerSorteoBlanco = Principal.class.getResource("/botones realizar sorteo.png");
        URL urlHacerSorteoRojo = Principal.class.getResource("/botones realizar sorteo rojo.png");
        
        estrellaBlanca = new ImageIcon(img1);
        estrellaLLena = new ImageIcon(img2);
        
        btnEvalBlanco = new ImageIcon(urlEvalBlanco);
        btnEvalRojo = new ImageIcon(urlEvalRojo);
        
        btnRestBlanco = new ImageIcon(urlRstBlanco);
        btnRestRojo = new ImageIcon(urlRstRojo);
        
        btnInfoBlanco = new ImageIcon(urlInfoBlanco);
        btnInfoRojo = new ImageIcon(urlInfoRojo);
        
        btnSorteoBlanco = new ImageIcon(urlSorteoBlanco);
        btnSorteoRojo  = new ImageIcon(urlSorteoRojo);
        
        btnHacerSorteoBlanco = new ImageIcon(urlHacerSorteoBlanco);
        btnHacerSorteoRojo  = new ImageIcon(urlHacerSorteoRojo);
        
        btnSorteo.setIcon(new ImageIcon(btnSorteoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
        btnRealizarSorteo.setIcon(new ImageIcon(btnHacerSorteoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
        btnInformacion.setIcon(new ImageIcon(btnInfoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
        btnRestaurant.setIcon(new ImageIcon(btnRestBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
        btnEvaluacion.setIcon(new ImageIcon(btnEvalBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
        
        estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));

        paneles.removeAll();
        paneles.add(panelRestaurant);
        paneles.repaint();
        paneles.revalidate();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        paneles = new javax.swing.JPanel();
        panelInformacion = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        btnInfoRst = new javax.swing.JButton();
        btnInfoSorteo = new javax.swing.JButton();
        btnInfoEval = new javax.swing.JButton();
        informacion = new javax.swing.JPanel();
        informacionEval = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        listaEvalInfo = new javax.swing.JList();
        jLabel31 = new javax.swing.JLabel();
        puntuacionInfo = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        evalUsuarioInfo = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        reseñaInfo = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        infoEvaluacionRestaurant = new javax.swing.JLabel();
        informacionRestaurant = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listaRestInfo = new javax.swing.JList();
        nombreRstInfo = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        direRstInfo = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        tipoComidaInfo = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        horarioInfo = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        comentarios = new javax.swing.JList();
        jLabel27 = new javax.swing.JLabel();
        infoEstrella1 = new javax.swing.JButton();
        infoEstrella2 = new javax.swing.JButton();
        infoEstrella3 = new javax.swing.JButton();
        infoEstrella4 = new javax.swing.JButton();
        infoEstrella5 = new javax.swing.JButton();
        infoSorteo = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        listaSorteoInfo = new javax.swing.JList();
        jLabel28 = new javax.swing.JLabel();
        infoSorteoRst = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        fechaSorteoInfo = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        descSorteoInfo = new javax.swing.JLabel();
        panelRealizarSorteo = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaSorteos = new javax.swing.JList();
        jLabel17 = new javax.swing.JLabel();
        brtRealizarSorteo = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaGanadores = new javax.swing.JList();
        panelRestaurant = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tipoComida = new javax.swing.JComboBox();
        btnAceptarFicha = new javax.swing.JButton();
        cajaNombreRst = new javax.swing.JTextField();
        cajaDireccionRst = new javax.swing.JTextField();
        ventHorarios = new javax.swing.JButton();
        panelSorteo = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblCantGanadores = new javax.swing.JLabel();
        btnSuma = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        btnResta = new javax.swing.JButton();
        btnAceptarSorteo = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        comboRestaurant = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        cajaDescripcionSorteo = new javax.swing.JTextField();
        dateSorteo = new datechooser.beans.DateChooserCombo();
        panelEvaluacion = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        estrella1 = new javax.swing.JButton();
        estrella2 = new javax.swing.JButton();
        estrella3 = new javax.swing.JButton();
        estrella4 = new javax.swing.JButton();
        estrella5 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        cajaReseña = new javax.swing.JTextField();
        evalAnonima = new javax.swing.JCheckBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cajaNombreUsuario = new javax.swing.JTextField();
        cajaMailUsuario = new javax.swing.JTextField();
        btnAceptarEvaluacion = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        comboRestaurantEval = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        btnRestaurant = new javax.swing.JButton();
        btnSorteo = new javax.swing.JButton();
        btnEvaluacion = new javax.swing.JButton();
        btnRealizarSorteo = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        btnInformacion = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        paneles.setLayout(new java.awt.CardLayout());

        panelInformacion.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel23.setText("Información");

        btnInfoRst.setText("Restaurant");
        btnInfoRst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInfoRstActionPerformed(evt);
            }
        });

        btnInfoSorteo.setText("Sorteo");
        btnInfoSorteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInfoSorteoActionPerformed(evt);
            }
        });

        btnInfoEval.setText("Evaluación");
        btnInfoEval.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInfoEvalActionPerformed(evt);
            }
        });

        informacion.setLayout(new java.awt.CardLayout());

        informacionEval.setBackground(new java.awt.Color(255, 255, 255));

        listaEvalInfo.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaEvalInfo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaEvalInfoValueChanged(evt);
            }
        });
        jScrollPane6.setViewportView(listaEvalInfo);

        jLabel31.setText("Puntuación:");

        jLabel32.setText("Usuario:");

        jLabel33.setText("Reseña:");

        jLabel35.setText("Restaurant:");

        javax.swing.GroupLayout informacionEvalLayout = new javax.swing.GroupLayout(informacionEval);
        informacionEval.setLayout(informacionEvalLayout);
        informacionEvalLayout.setHorizontalGroup(
            informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informacionEvalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 710, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(informacionEvalLayout.createSequentialGroup()
                        .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel33, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE))
                            .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(infoEvaluacionRestaurant, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(evalUsuarioInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(puntuacionInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(reseñaInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)))))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        informacionEvalLayout.setVerticalGroup(
            informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informacionEvalLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(puntuacionInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(evalUsuarioInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                    .addComponent(reseñaInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27)
                .addGroup(informacionEvalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(infoEvaluacionRestaurant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(69, Short.MAX_VALUE))
        );

        informacion.add(informacionEval, "card4");

        informacionRestaurant.setBackground(new java.awt.Color(255, 255, 255));

        listaRestInfo.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaRestInfo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaRestInfoValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(listaRestInfo);

        jLabel24.setText("Dirección:");

        jLabel25.setText("Tipo de comida:");

        jLabel26.setText("Horario:");

        jScrollPane4.setViewportView(comentarios);

        jLabel27.setText("Comentarios:");

        infoEstrella1.setBorderPainted(false);
        infoEstrella1.setContentAreaFilled(false);

        infoEstrella2.setBorderPainted(false);
        infoEstrella2.setContentAreaFilled(false);

        infoEstrella3.setBorderPainted(false);
        infoEstrella3.setContentAreaFilled(false);

        infoEstrella4.setBorderPainted(false);
        infoEstrella4.setContentAreaFilled(false);

        infoEstrella5.setBorderPainted(false);
        infoEstrella5.setContentAreaFilled(false);

        javax.swing.GroupLayout informacionRestaurantLayout = new javax.swing.GroupLayout(informacionRestaurant);
        informacionRestaurant.setLayout(informacionRestaurantLayout);
        informacionRestaurantLayout.setHorizontalGroup(
            informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informacionRestaurantLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(informacionRestaurantLayout.createSequentialGroup()
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane4))
                    .addGroup(informacionRestaurantLayout.createSequentialGroup()
                        .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(informacionRestaurantLayout.createSequentialGroup()
                                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(42, 42, 42)
                                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(direRstInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tipoComidaInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(horarioInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 571, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(informacionRestaurantLayout.createSequentialGroup()
                                .addGap(96, 96, 96)
                                .addComponent(nombreRstInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(infoEstrella1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(infoEstrella2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(infoEstrella3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(infoEstrella4, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(infoEstrella5, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        informacionRestaurantLayout.setVerticalGroup(
            informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informacionRestaurantLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreRstInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(infoEstrella1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoEstrella2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoEstrella3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoEstrella4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoEstrella5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(informacionRestaurantLayout.createSequentialGroup()
                        .addComponent(direRstInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tipoComidaInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(informacionRestaurantLayout.createSequentialGroup()
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(horarioInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(informacionRestaurantLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(jLabel27))
                .addGap(29, 29, 29))
        );

        informacion.add(informacionRestaurant, "card2");

        infoSorteo.setBackground(new java.awt.Color(255, 255, 255));

        listaSorteoInfo.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaSorteoInfo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaSorteoInfoValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(listaSorteoInfo);

        jLabel28.setText("Restaurant:");

        jLabel29.setText("Fecha:");

        jLabel30.setText("Descipción:");

        descSorteoInfo.setToolTipText("");
        descSorteoInfo.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout infoSorteoLayout = new javax.swing.GroupLayout(infoSorteo);
        infoSorteo.setLayout(infoSorteoLayout);
        infoSorteoLayout.setHorizontalGroup(
            infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoSorteoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 691, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(infoSorteoLayout.createSequentialGroup()
                        .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel30, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(infoSorteoRst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(fechaSorteoInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE))
                            .addComponent(descSorteoInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        infoSorteoLayout.setVerticalGroup(
            infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoSorteoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(infoSorteoRst, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechaSorteoInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(infoSorteoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(infoSorteoLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(infoSorteoLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(descSorteoInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        informacion.add(infoSorteo, "card3");

        javax.swing.GroupLayout panelInformacionLayout = new javax.swing.GroupLayout(panelInformacion);
        panelInformacion.setLayout(panelInformacionLayout);
        panelInformacionLayout.setHorizontalGroup(
            panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInformacionLayout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(btnInfoRst)
                .addGap(98, 98, 98)
                .addGroup(panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addGroup(panelInformacionLayout.createSequentialGroup()
                        .addComponent(btnInfoSorteo, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(95, 95, 95)
                        .addComponent(btnInfoEval, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(informacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelInformacionLayout.setVerticalGroup(
            panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInformacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel23)
                .addGap(15, 15, 15)
                .addGroup(panelInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInfoRst)
                    .addComponent(btnInfoSorteo)
                    .addComponent(btnInfoEval))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(informacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        paneles.add(panelInformacion, "card6");

        panelRealizarSorteo.setBackground(new java.awt.Color(255, 255, 255));
        panelRealizarSorteo.setLayout(null);

        jLabel16.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel16.setText("Realizar sorteo");
        panelRealizarSorteo.add(jLabel16);
        jLabel16.setBounds(290, 30, 220, 35);

        listaSorteos.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listaSorteos);

        panelRealizarSorteo.add(jScrollPane1);
        jScrollPane1.setBounds(310, 120, 286, 105);

        jLabel17.setText("Sorteo:");
        panelRealizarSorteo.add(jLabel17);
        jLabel17.setBounds(150, 130, 48, 14);

        brtRealizarSorteo.setText("Realizar sorteo");
        brtRealizarSorteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brtRealizarSorteoActionPerformed(evt);
            }
        });
        panelRealizarSorteo.add(brtRealizarSorteo);
        brtRealizarSorteo.setBounds(370, 290, 160, 23);

        jLabel18.setText("Ganadores:");
        panelRealizarSorteo.add(jLabel18);
        jLabel18.setBounds(160, 380, 100, 14);

        jScrollPane2.setViewportView(listaGanadores);

        panelRealizarSorteo.add(jScrollPane2);
        jScrollPane2.setBounds(310, 380, 292, 94);

        paneles.add(panelRealizarSorteo, "card5");

        panelRestaurant.setBackground(new java.awt.Color(255, 255, 255));
        panelRestaurant.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel1.setText("Ficha restaurant");
        panelRestaurant.add(jLabel1);
        jLabel1.setBounds(290, 30, 250, 35);

        jLabel2.setText("Nombre:");
        panelRestaurant.add(jLabel2);
        jLabel2.setBounds(180, 90, 75, 14);

        jLabel3.setText("Dirección:");
        panelRestaurant.add(jLabel3);
        jLabel3.setBounds(180, 150, 59, 14);

        jLabel4.setText("Horario:");
        panelRestaurant.add(jLabel4);
        jLabel4.setBounds(180, 220, 49, 14);

        jLabel7.setText("Tipo de comida:");
        panelRestaurant.add(jLabel7);
        jLabel7.setBounds(180, 330, 100, 14);

        tipoComida.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Calzones", "Carnes", "Celiacos", "Chivitos", "Comida Armenia", "Comida China", "Comida Japonesa", "Comida Mexicana", "Comida Peruana", "Comida Thai", "Comida Vegana", "Comida Vegetariana", "Comida Venezolana", "Comida Arabe", "Crepes", "Empanadas", "Ensaladas", "Hamburguesas", "Lehmeyun", "Milanesas", "Panchos", "Parrilla", "Pasta", "Pescados", "Pizzas", "Sushi" }));
        tipoComida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipoComidaActionPerformed(evt);
            }
        });
        panelRestaurant.add(tipoComida);
        tipoComida.setBounds(400, 320, 150, 20);

        btnAceptarFicha.setText("Aceptar");
        btnAceptarFicha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarFichaActionPerformed(evt);
            }
        });
        panelRestaurant.add(btnAceptarFicha);
        btnAceptarFicha.setBounds(290, 430, 100, 23);

        cajaNombreRst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaNombreRstActionPerformed(evt);
            }
        });
        panelRestaurant.add(cajaNombreRst);
        cajaNombreRst.setBounds(400, 90, 150, 20);
        panelRestaurant.add(cajaDireccionRst);
        cajaDireccionRst.setBounds(400, 150, 150, 20);

        ventHorarios.setText("Ingresar horarios");
        ventHorarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ventHorariosActionPerformed(evt);
            }
        });
        panelRestaurant.add(ventHorarios);
        ventHorarios.setBounds(400, 220, 150, 23);

        paneles.add(panelRestaurant, "card5");

        panelSorteo.setBackground(new java.awt.Color(255, 255, 255));
        panelSorteo.setLayout(null);

        jLabel11.setText("Cantidad de ganadores:");
        panelSorteo.add(jLabel11);
        jLabel11.setBounds(180, 90, 150, 14);

        jLabel10.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel10.setText("Sorteo");
        panelSorteo.add(jLabel10);
        jLabel10.setBounds(370, 10, 150, 35);

        lblCantGanadores.setText("1");
        panelSorteo.add(lblCantGanadores);
        lblCantGanadores.setBounds(360, 90, 51, 14);

        btnSuma.setText("+");
        btnSuma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSumaActionPerformed(evt);
            }
        });
        panelSorteo.add(btnSuma);
        btnSuma.setBounds(440, 90, 50, 23);

        jLabel12.setText("Fecha:");
        panelSorteo.add(jLabel12);
        jLabel12.setBounds(180, 150, 80, 14);

        btnResta.setText("-");
        btnResta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaActionPerformed(evt);
            }
        });
        panelSorteo.add(btnResta);
        btnResta.setBounds(520, 90, 60, 23);

        btnAceptarSorteo.setText("Aceptar");
        btnAceptarSorteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarSorteoActionPerformed(evt);
            }
        });
        panelSorteo.add(btnAceptarSorteo);
        btnAceptarSorteo.setBounds(370, 360, 100, 23);

        jLabel13.setText("Restaurant:");
        panelSorteo.add(jLabel13);
        jLabel13.setBounds(180, 200, 100, 14);

        comboRestaurant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRestaurantActionPerformed(evt);
            }
        });
        panelSorteo.add(comboRestaurant);
        comboRestaurant.setBounds(360, 200, 220, 20);

        jLabel14.setText("Descripción:");
        panelSorteo.add(jLabel14);
        jLabel14.setBounds(180, 260, 90, 14);
        panelSorteo.add(cajaDescripcionSorteo);
        cajaDescripcionSorteo.setBounds(360, 250, 220, 43);

        dateSorteo.setCurrentView(new datechooser.view.appearance.AppearancesList("Swing",
            new datechooser.view.appearance.ViewAppearance("custom",
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    true,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 255),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(128, 128, 128),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(255, 0, 0),
                    false,
                    false,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                (datechooser.view.BackRenderer)null,
                false,
                true)));
    panelSorteo.add(dateSorteo);
    dateSorteo.setBounds(360, 150, 220, 20);

    paneles.add(panelSorteo, "card2");

    panelEvaluacion.setBackground(new java.awt.Color(255, 255, 255));
    panelEvaluacion.setLayout(null);

    jLabel5.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
    jLabel5.setText("Evaluación de Restaurant");
    panelEvaluacion.add(jLabel5);
    jLabel5.setBounds(260, 20, 370, 25);

    estrella1.setBorderPainted(false);
    estrella1.setContentAreaFilled(false);
    estrella1.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            estrella1MouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            estrella1MouseExited(evt);
        }
    });
    estrella1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            estrella1ActionPerformed(evt);
        }
    });
    panelEvaluacion.add(estrella1);
    estrella1.setBounds(160, 70, 45, 35);

    estrella2.setBorderPainted(false);
    estrella2.setContentAreaFilled(false);
    estrella2.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            estrella2MouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            estrella2MouseExited(evt);
        }
    });
    estrella2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            estrella2ActionPerformed(evt);
        }
    });
    panelEvaluacion.add(estrella2);
    estrella2.setBounds(250, 70, 45, 35);

    estrella3.setBorderPainted(false);
    estrella3.setContentAreaFilled(false);
    estrella3.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            estrella3MouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            estrella3MouseExited(evt);
        }
    });
    estrella3.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            estrella3ActionPerformed(evt);
        }
    });
    panelEvaluacion.add(estrella3);
    estrella3.setBounds(330, 70, 45, 35);

    estrella4.setBorderPainted(false);
    estrella4.setContentAreaFilled(false);
    estrella4.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            estrella4MouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            estrella4MouseExited(evt);
        }
    });
    estrella4.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            estrella4ActionPerformed(evt);
        }
    });
    panelEvaluacion.add(estrella4);
    estrella4.setBounds(420, 70, 45, 35);

    estrella5.setBorderPainted(false);
    estrella5.setContentAreaFilled(false);
    estrella5.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            estrella5MouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            estrella5MouseExited(evt);
        }
    });
    estrella5.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            estrella5ActionPerformed(evt);
        }
    });
    panelEvaluacion.add(estrella5);
    estrella5.setBounds(510, 70, 45, 35);

    jLabel6.setText("Reseña : ");
    panelEvaluacion.add(jLabel6);
    jLabel6.setBounds(150, 140, 70, 14);
    panelEvaluacion.add(cajaReseña);
    cajaReseña.setBounds(310, 120, 200, 50);

    evalAnonima.setBackground(new java.awt.Color(255, 255, 255));
    evalAnonima.setText("Anónima");
    evalAnonima.addChangeListener(new javax.swing.event.ChangeListener() {
        public void stateChanged(javax.swing.event.ChangeEvent evt) {
            evalAnonimaStateChanged(evt);
        }
    });
    panelEvaluacion.add(evalAnonima);
    evalAnonima.setBounds(140, 260, 80, 23);

    jLabel8.setText("Nombre:");
    panelEvaluacion.add(jLabel8);
    jLabel8.setBounds(310, 260, 80, 14);

    jLabel9.setText("Mail:");
    panelEvaluacion.add(jLabel9);
    jLabel9.setBounds(310, 310, 50, 14);
    panelEvaluacion.add(cajaNombreUsuario);
    cajaNombreUsuario.setBounds(410, 260, 140, 20);
    panelEvaluacion.add(cajaMailUsuario);
    cajaMailUsuario.setBounds(410, 310, 140, 20);

    btnAceptarEvaluacion.setText("Aceptar");
    btnAceptarEvaluacion.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnAceptarEvaluacionActionPerformed(evt);
        }
    });
    panelEvaluacion.add(btnAceptarEvaluacion);
    btnAceptarEvaluacion.setBounds(330, 390, 100, 23);

    jLabel15.setText("Restaurant:");
    panelEvaluacion.add(jLabel15);
    jLabel15.setBounds(150, 210, 80, 14);

    panelEvaluacion.add(comboRestaurantEval);
    comboRestaurantEval.setBounds(310, 200, 180, 20);

    paneles.add(panelEvaluacion, "card4");

    jPanel1.setBackground(new java.awt.Color(255, 255, 255));

    btnRestaurant.setBorderPainted(false);
    btnRestaurant.setContentAreaFilled(false);
    btnRestaurant.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            btnRestaurantMouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            btnRestaurantMouseExited(evt);
        }
    });
    btnRestaurant.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRestaurantActionPerformed(evt);
        }
    });

    btnSorteo.setBorderPainted(false);
    btnSorteo.setContentAreaFilled(false);
    btnSorteo.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            btnSorteoMouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            btnSorteoMouseExited(evt);
        }
    });
    btnSorteo.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnSorteoActionPerformed(evt);
        }
    });

    btnEvaluacion.setBorderPainted(false);
    btnEvaluacion.setContentAreaFilled(false);
    btnEvaluacion.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnEvaluacionMouseClicked(evt);
        }
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            btnEvaluacionMouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            btnEvaluacionMouseExited(evt);
        }
    });
    btnEvaluacion.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnEvaluacionActionPerformed(evt);
        }
    });

    btnRealizarSorteo.setBorderPainted(false);
    btnRealizarSorteo.setContentAreaFilled(false);
    btnRealizarSorteo.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            btnRealizarSorteoMouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            btnRealizarSorteoMouseExited(evt);
        }
    });
    btnRealizarSorteo.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRealizarSorteoActionPerformed(evt);
        }
    });

    jLabel19.setText("  Ficha restaurant");

    jLabel20.setText("Evaluación");

    jLabel21.setText("Sorteo");

    jLabel22.setText("Realizar sorteo");

    btnInformacion.setBorderPainted(false);
    btnInformacion.setContentAreaFilled(false);
    btnInformacion.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            btnInformacionMouseEntered(evt);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
            btnInformacionMouseExited(evt);
        }
    });
    btnInformacion.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnInformacionActionPerformed(evt);
        }
    });

    jLabel34.setText("Información");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnEvaluacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnSorteo, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                                    .addComponent(btnInformacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnRealizarSorteo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(10, 10, 10)
                                    .addComponent(jLabel22))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(34, 34, 34)
                            .addComponent(jLabel20))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(32, 32, 32)
                            .addComponent(jLabel34))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(jLabel21)))
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
                .addComponent(btnRestaurant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(6, 6, 6)
            .addComponent(btnRestaurant, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLabel19)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnEvaluacion, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel20)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnSorteo, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(9, 9, 9)
            .addComponent(jLabel21)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnRealizarSorteo, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel22)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnInformacion, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLabel34)
            .addGap(24, 24, 24))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(paneles, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(paneles, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE)
    );

    pack();
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnRestaurantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurantActionPerformed
        paneles.removeAll();
        paneles.add(panelRestaurant);
        paneles.repaint();
        paneles.revalidate();
        ingresoFecha = false;
    }//GEN-LAST:event_btnRestaurantActionPerformed

    private void btnSorteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSorteoActionPerformed
        paneles.removeAll();
        paneles.add(panelSorteo);
        paneles.repaint();
        paneles.revalidate();
        
        try{
            comboRestaurant.removeAllItems();
        }
        catch(Exception e){
            
        }
        
        for(int i = 0; i < modelo.getListaRestaurant().size(); i++){
            Restaurant agregar = modelo.getListaRestaurant().get(i);
            comboRestaurant.addItem(agregar);
        }
    }//GEN-LAST:event_btnSorteoActionPerformed

    private void btnEvaluacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEvaluacionActionPerformed
        paneles.removeAll();
        paneles.add(panelEvaluacion);
        paneles.repaint();
        paneles.revalidate();
         
        try{
            comboRestaurantEval.removeAllItems();
        }
        catch(Exception e){
            
        }
        
        for(int i = 0; i < modelo.getListaRestaurant().size(); i++){
            Restaurant agregar = modelo.getListaRestaurant().get(i);
            comboRestaurantEval.addItem(agregar);
        }
    }//GEN-LAST:event_btnEvaluacionActionPerformed

    private void btnRealizarSorteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRealizarSorteoActionPerformed
        paneles.removeAll();
        paneles.add(panelRealizarSorteo);
        paneles.repaint();
        paneles.revalidate();
        
        listaSorteos.setListData(modelo.getListaSorteo().toArray());
    }//GEN-LAST:event_btnRealizarSorteoActionPerformed

    private void btnAceptarFichaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarFichaActionPerformed
       
        if (ingresoFecha){
            Restaurant unRestaurant = new Restaurant();
            
            unRestaurant.setHorario(restaurantAux.getHorario());
            String nombreRst = cajaNombreRst.getText();
            String direccionRst = cajaDireccionRst.getText();
            String elTipoComida = tipoComida.getSelectedItem().toString();
            
            unRestaurant.setNombre(nombreRst);
            unRestaurant.setDireccion(direccionRst);
            unRestaurant.setTipoComida(elTipoComida);

            boolean nombreOk = !unRestaurant.esDatoVacio(nombreRst);
            boolean direccionOk = !unRestaurant.esDatoVacio(direccionRst);

            if (nombreOk && direccionOk ){
                if (!modelo.getListaRestaurant().contains(unRestaurant)){
                         modelo.getListaRestaurant().add(unRestaurant);
                         JOptionPane.showMessageDialog(rootPane,"Restaurant ingresado con exito", "Ingreso exitoso", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    int posicion = modelo.getListaRestaurant().indexOf(unRestaurant);
                    Restaurant elRestaurant = modelo.getListaRestaurant().get(posicion);
                    elRestaurant.setHorario(horarios);
                    elRestaurant.setDireccion(direccionRst);
                    unRestaurant.setTipoComida(elTipoComida);
                    
                    
                   JOptionPane.showMessageDialog(rootPane,"Restaurante ya registrado. Se modificaron los datos", "Informacion", JOptionPane.INFORMATION_MESSAGE); 
                }
            }
            else{
               JOptionPane.showMessageDialog(rootPane,"No se puede registrar restaurant", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        else{
            JOptionPane.showMessageDialog(rootPane,"No se puede registrar restaurant", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAceptarFichaActionPerformed

    private void ventHorariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ventHorariosActionPerformed
        restaurantAux = new Restaurant();
        VentanaHorarios ventana = new VentanaHorarios(restaurantAux);
        ventana.setVisible(true);
        ingresoFecha = horarios.trim().isEmpty();
    }//GEN-LAST:event_ventHorariosActionPerformed

    private void estrella3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estrella3ActionPerformed
        estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        debeCambiar = true;
        cantEstrellas = 3;
    }//GEN-LAST:event_estrella3ActionPerformed

    private void estrella1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella1MouseEntered
      estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
      
    }//GEN-LAST:event_estrella1MouseEntered

    private void estrella1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella1MouseExited
        if (!debeCambiar)
            estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_estrella1MouseExited

    private void estrella1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estrella1ActionPerformed
        estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));

        cantEstrellas = 1;
        debeCambiar = true;
    }//GEN-LAST:event_estrella1ActionPerformed

    private void estrella2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella2MouseEntered
         if (!debeCambiar){
            estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
         }
    }//GEN-LAST:event_estrella2MouseEntered

    private void estrella2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella2MouseExited
        if (!debeCambiar){
            estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella2MouseExited

    private void estrella2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estrella2ActionPerformed
       estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
       estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
       estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
       estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
       estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));

        cantEstrellas = 2;
        debeCambiar = true;
    }//GEN-LAST:event_estrella2ActionPerformed

    private void estrella3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella3MouseEntered
       if(!debeCambiar){
         estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
         estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
         estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella3MouseEntered

    private void estrella3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella3MouseExited
        if (!debeCambiar){
             estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella3MouseExited

    private void estrella4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella4MouseEntered
        if (!debeCambiar){
             estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella4MouseEntered

    private void estrella4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella4MouseExited
        if (!debeCambiar){
             estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella4MouseExited

    private void estrella4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estrella4ActionPerformed
        estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        debeCambiar = true;
        cantEstrellas = 4;
    }//GEN-LAST:event_estrella4ActionPerformed

    private void estrella5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella5MouseEntered
       if (!debeCambiar){
             estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella5.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella5MouseEntered

    private void estrella5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_estrella5MouseExited
        if (!debeCambiar){
             estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
             estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        }
    }//GEN-LAST:event_estrella5MouseExited

    private void estrella5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estrella5ActionPerformed
        estrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        estrella5.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
        debeCambiar = true;
        cantEstrellas = 5;
          
    }//GEN-LAST:event_estrella5ActionPerformed

    private void evalAnonimaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_evalAnonimaStateChanged
        if(evalAnonima.isSelected()){
            cajaNombreUsuario.setEnabled(false);
            cajaMailUsuario.setEnabled(false);
        }
        else{
           cajaNombreUsuario.setEnabled(true);
           cajaMailUsuario.setEnabled(true); 
        }
    }//GEN-LAST:event_evalAnonimaStateChanged

    private void btnAceptarEvaluacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarEvaluacionActionPerformed
       boolean seleccionoEstrella = (cantEstrellas != 0);
       boolean esAnonima = evalAnonima.isSelected();
       
       if ((!esAnonima) && (!modelo.getListaRestaurant().isEmpty())){
           Usuario elUsuario = new Usuario();
           if (elUsuario.esMailValido(cajaMailUsuario.getText())){
               
               elUsuario.setMail(cajaMailUsuario.getText());
               elUsuario.setNombre(cajaNombreUsuario.getText());
               
               if(!modelo.getListaUsuarios().contains(elUsuario)){
                   modelo.getListaUsuarios().add(elUsuario);
               }
               if (seleccionoEstrella){
                   
                    Evaluacion laEvaluacion = new Evaluacion();
                    
                    String nombreRestaurant = comboRestaurantEval.getSelectedItem().toString();
                    Restaurant elResAux = new Restaurant();
                    elResAux.setNombre(nombreRestaurant);
                    int posicionEnElArray = modelo.getListaRestaurant().indexOf(elResAux);
                    
                    Restaurant elRestaurant = modelo.getListaRestaurant().get(posicionEnElArray);
                    
                    laEvaluacion.setEstrellas(cantEstrellas);
                    laEvaluacion.setEsAnonima(esAnonima);
                    laEvaluacion.setUsuario(elUsuario);
                    laEvaluacion.setRestaurant(elRestaurant);
                    elRestaurant.agregarPuntacion(cantEstrellas);
                    
                    if (cajaReseña.getText().isEmpty()){
                        laEvaluacion.setReseña("Sin reseña");
                    }
                    else{
                        laEvaluacion.setReseña(cajaReseña.getText());
                    }
                    debeCambiar = false;
                    cantEstrellas = 0;
                    estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                    estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                    estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                    estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                    estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                    
                    modelo.getListaEvaluacion().add(laEvaluacion);
                    JOptionPane.showMessageDialog(rootPane,"Evaluación ingresada con éxito", "Ingreso exitoso", JOptionPane.INFORMATION_MESSAGE);
                }
               else{
                    JOptionPane.showMessageDialog(rootPane,"Debe ingresar puntuación estrellas", "Error", JOptionPane.ERROR_MESSAGE);
               }
           
            } 
           else{
                JOptionPane.showMessageDialog(rootPane,"Mail no válido", "Error", JOptionPane.ERROR_MESSAGE);
           }
       }
       else{
           if(modelo.getListaRestaurant().isEmpty()){
                JOptionPane.showMessageDialog(rootPane,"Debe haber registrado al menos un restaurant", "Error", JOptionPane.ERROR_MESSAGE);
           }
           else{
                if (seleccionoEstrella){
                         Evaluacion laEvaluacion = new Evaluacion();
                         laEvaluacion.setEstrellas(cantEstrellas);
                         laEvaluacion.setEsAnonima(esAnonima);
                         if (cajaReseña.getText().isEmpty()){
                                laEvaluacion.setReseña("Sin reseña");
                         }
                         else{
                                laEvaluacion.setReseña(cajaReseña.getText());
                        }
                         laEvaluacion.setUsuario(new Usuario("Anónima", "Anónima"));
                         String nombreRestaurant = comboRestaurantEval.getSelectedItem().toString();
                         Restaurant elResAux = new Restaurant();
                         elResAux.setNombre(nombreRestaurant);
                         int posicionEnElArray = modelo.getListaRestaurant().indexOf(elResAux);

                         Restaurant elRestaurant = modelo.getListaRestaurant().get(posicionEnElArray);
                    
                         laEvaluacion.setRestaurant(elRestaurant);
                         
                         estrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                         estrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                         estrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                         estrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                         estrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                         debeCambiar = false;
                         
                         
                         
                         elRestaurant.agregarPuntacion(cantEstrellas);
                         cantEstrellas = 0;
                         
                         modelo.getListaEvaluacion().add(laEvaluacion);
                         JOptionPane.showMessageDialog(rootPane,"Evaluacion ingresada con exito", "Ingreso exitoso", JOptionPane.INFORMATION_MESSAGE);
                     }
                    else{
                         JOptionPane.showMessageDialog(rootPane,"Debe ingresar puntuacion estrellas", "Error", JOptionPane.ERROR_MESSAGE);
                    }
           }
    }//GEN-LAST:event_btnAceptarEvaluacionActionPerformed
    }
    private void btnSumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSumaActionPerformed
       lblCantGanadores.setText((Integer.parseInt(lblCantGanadores.getText())+1)+"");
    }//GEN-LAST:event_btnSumaActionPerformed

    private void btnRestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaActionPerformed
        if(!lblCantGanadores.getText().equals("1"))
            lblCantGanadores.setText((Integer.parseInt(lblCantGanadores.getText())-1)+"");
    }//GEN-LAST:event_btnRestaActionPerformed

    private void btnAceptarSorteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarSorteoActionPerformed
        boolean descripcionOk = !cajaDescripcionSorteo.getText().isEmpty();
        if (descripcionOk && (!modelo.getListaRestaurant().isEmpty())){
            Sorteo elSorteo = new Sorteo();
            String cantGanadores = lblCantGanadores.getText();
            
            String lafecha = dateSorteo.getText();
            
            String descripcion = cajaDescripcionSorteo.getText();
            
            String nombreRestaurant = comboRestaurant.getSelectedItem().toString();
            Restaurant restAux = new Restaurant();
            restAux.setNombre(nombreRestaurant);
            int posicionEnElArray = modelo.getListaRestaurant().indexOf(restAux);
            Restaurant elRestaurant = modelo.getListaRestaurant().get(posicionEnElArray);
            
            elSorteo.setDescripcion(descripcion);
            elSorteo.setFecha(lafecha);
            elSorteo.setNumeroGanadores(Integer.parseInt(cantGanadores));
            elSorteo.setRestaurant(elRestaurant);
            
            modelo.getListaSorteo().add(elSorteo);
            
            JOptionPane.showMessageDialog(rootPane,"Sorteo registrado con exito", "Registro exitoso", JOptionPane.INFORMATION_MESSAGE);
        }
        else{
            if (modelo.getListaRestaurant().isEmpty()){
                JOptionPane.showMessageDialog(rootPane,"Debe haber registrado al menos un restaurant", "Error de registro", JOptionPane.ERROR_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(rootPane,"Debe ingresar una descripcion", "Error de registro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnAceptarSorteoActionPerformed

    private void comboRestaurantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRestaurantActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboRestaurantActionPerformed

    private void brtRealizarSorteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brtRealizarSorteoActionPerformed
        Sorteo sorteo = (Sorteo) listaSorteos.getSelectedValue();
        if (sorteo != null){
            modelo.usuariosParticipantes(sorteo, sorteo.getRestaurant());
        
            if(sorteo.getNumeroGanadores() <= sorteo.getUsuario().size()){
                sorteo.realizarSorteo();
             
                JOptionPane.showMessageDialog(rootPane,"Sorteo realizado con exito", "Sorteo realizado", JOptionPane.INFORMATION_MESSAGE);
             
                 int posicionSorteo = modelo.getListaSorteo().indexOf(sorteo);
                modelo.getListaSorteo().remove(modelo.getListaSorteo().get(posicionSorteo));
             
                 listaSorteos.setListData(modelo.getListaSorteo().toArray());
                listaGanadores.setListData(sorteo.getGanadores().toArray());
            }
            else{
                JOptionPane.showMessageDialog(rootPane,"El sorteo debe tener mas concursantes que premios", "Sorteo no realizado", JOptionPane.ERROR_MESSAGE);
            }
        }
        else{
            JOptionPane.showMessageDialog(rootPane,"Debe seleccionar un sorteo", "Sorteo no realizado", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_brtRealizarSorteoActionPerformed

    private void tipoComidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipoComidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tipoComidaActionPerformed

    private void cajaNombreRstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaNombreRstActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaNombreRstActionPerformed

    private void btnInfoRstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInfoRstActionPerformed
        informacion.removeAll();
        informacion.add(informacionRestaurant);
        informacion.repaint();
        informacion.revalidate();
        
        
        listaRestInfo.setListData(modelo.getListaRestaurant().toArray());
    }//GEN-LAST:event_btnInfoRstActionPerformed

    private void btnInformacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInformacionActionPerformed
        paneles.removeAll();
        paneles.add(panelInformacion);
        informacion.removeAll();
        paneles.repaint();
        paneles.revalidate();
        
    }//GEN-LAST:event_btnInformacionActionPerformed

    private void listaRestInfoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaRestInfoValueChanged
        if (!listaRestInfo.isSelectionEmpty()){
            String elNombre = listaRestInfo.getSelectedValue().toString();
            Restaurant restAux = new Restaurant();
            restAux.setNombre(elNombre);

            int posicion = modelo.getListaRestaurant().indexOf(restAux);
            Restaurant elRestaurant = modelo.getListaRestaurant().get(posicion);
            nombreRstInfo.setText(elRestaurant.getNombre());
            direRstInfo.setText(elRestaurant.getDireccion());
            tipoComidaInfo.setText(elRestaurant.getTipoComida());
            horarioInfo.setText(elRestaurant.getHorario());
            comentarios.setListData(modelo.evaluacionPorRestaurant(restAux).toArray());

            int cantEstr = elRestaurant.getPuntacion();
            infoEstrella1.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            infoEstrella2.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            infoEstrella3.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            infoEstrella4.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
            infoEstrella5.setIcon(new ImageIcon(estrellaBlanca.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));

            switch (cantEstr){
                case 1: infoEstrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        break;
                case 2: infoEstrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        break;
                case 3: infoEstrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        break;
                case 4: infoEstrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        break;
                case 5: infoEstrella1.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella2.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella3.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella4.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        infoEstrella5.setIcon(new ImageIcon(estrellaLLena.getImage().getScaledInstance(70 / 2, 70 / 2, Image.SCALE_DEFAULT)));
                        break;
                default: break;

            }
        }   
    }//GEN-LAST:event_listaRestInfoValueChanged

    private void btnInfoSorteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInfoSorteoActionPerformed
        informacion.removeAll();
        informacion.add(infoSorteo);
        informacion.repaint();
        informacion.revalidate();
        
        listaSorteoInfo.setListData(modelo.getListaSorteo().toArray());
    }//GEN-LAST:event_btnInfoSorteoActionPerformed

    private void listaSorteoInfoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaSorteoInfoValueChanged
       if (!listaSorteoInfo.isSelectionEmpty()){
           String nombreSorteo = listaSorteoInfo.getSelectedValue().toString();
           Sorteo sorteoAux = new Sorteo();
           Restaurant restAux = new Restaurant();
           restAux.setNombre(nombreSorteo);
           sorteoAux.setRestaurant(restAux);
           int posicion = modelo.getListaSorteo().indexOf(sorteoAux);
           Sorteo elSorteo = modelo.getListaSorteo().get(posicion);

           infoSorteoRst.setText(elSorteo.getRestaurant().toString());
           fechaSorteoInfo.setText(elSorteo.getFecha());
           descSorteoInfo.setText(elSorteo.getDescripcion());
       
       }  
    }//GEN-LAST:event_listaSorteoInfoValueChanged

    private void btnInfoEvalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInfoEvalActionPerformed
        informacion.removeAll();
        informacion.add(informacionEval);
        informacion.repaint();
        informacion.revalidate();
        
        listaEvalInfo.setListData(modelo.getListaEvaluacion().toArray());
    }//GEN-LAST:event_btnInfoEvalActionPerformed

    private void listaEvalInfoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaEvalInfoValueChanged
       if (!listaEvalInfo.isSelectionEmpty()){
            String nombre = listaEvalInfo.getSelectedValue().toString();
            Evaluacion evalAux = new Evaluacion();
            String[] separarDato = nombre.split(":");
            Usuario usu = new Usuario();
            usu.setMail(separarDato[0].trim());
            
            evalAux.setReseña(separarDato[1].trim());
            evalAux.setUsuario(usu);
            int posicion = modelo.getListaEvaluacion().indexOf(evalAux);
            
            Evaluacion laEval = modelo.getListaEvaluacion().get(posicion);
            
            puntuacionInfo.setText(laEval.getEstrellas()+ " ");
            evalUsuarioInfo.setText(laEval.getUsuario().toString());
            reseñaInfo.setText(laEval.getReseña());
            infoEvaluacionRestaurant.setText(laEval.getRestaurant().toString());
       
       }
    }//GEN-LAST:event_listaEvalInfoValueChanged

    private void btnRestaurantMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRestaurantMouseEntered
        btnRestaurant.setIcon(new ImageIcon(btnRestRojo.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnRestaurantMouseEntered

    private void btnRestaurantMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRestaurantMouseExited
        btnRestaurant.setIcon(new ImageIcon(btnRestBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnRestaurantMouseExited

    private void btnEvaluacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEvaluacionMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEvaluacionMouseClicked

    private void btnEvaluacionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEvaluacionMouseEntered
        btnEvaluacion.setIcon(new ImageIcon(btnEvalRojo.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnEvaluacionMouseEntered

    private void btnEvaluacionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEvaluacionMouseExited
        btnEvaluacion.setIcon(new ImageIcon(btnEvalBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnEvaluacionMouseExited

    private void btnInformacionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnInformacionMouseEntered
        btnInformacion.setIcon(new ImageIcon(btnInfoRojo.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnInformacionMouseEntered

    private void btnInformacionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnInformacionMouseExited
        btnInformacion.setIcon(new ImageIcon(btnInfoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnInformacionMouseExited

    private void btnRealizarSorteoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRealizarSorteoMouseEntered
        btnRealizarSorteo.setIcon(new ImageIcon(btnHacerSorteoRojo.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnRealizarSorteoMouseEntered

    private void btnRealizarSorteoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRealizarSorteoMouseExited
        btnRealizarSorteo.setIcon(new ImageIcon(btnHacerSorteoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnRealizarSorteoMouseExited

    private void btnSorteoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSorteoMouseEntered
        btnSorteo.setIcon(new ImageIcon(btnSorteoRojo.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnSorteoMouseEntered

    private void btnSorteoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSorteoMouseExited
        btnSorteo.setIcon(new ImageIcon(btnSorteoBlanco.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
    }//GEN-LAST:event_btnSorteoMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton brtRealizarSorteo;
    private javax.swing.JButton btnAceptarEvaluacion;
    private javax.swing.JButton btnAceptarFicha;
    private javax.swing.JButton btnAceptarSorteo;
    private javax.swing.JButton btnEvaluacion;
    private javax.swing.JButton btnInfoEval;
    private javax.swing.JButton btnInfoRst;
    private javax.swing.JButton btnInfoSorteo;
    private javax.swing.JButton btnInformacion;
    private javax.swing.JButton btnRealizarSorteo;
    private javax.swing.JButton btnResta;
    private javax.swing.JButton btnRestaurant;
    private javax.swing.JButton btnSorteo;
    private javax.swing.JButton btnSuma;
    private javax.swing.JTextField cajaDescripcionSorteo;
    private javax.swing.JTextField cajaDireccionRst;
    private javax.swing.JTextField cajaMailUsuario;
    private javax.swing.JTextField cajaNombreRst;
    private javax.swing.JTextField cajaNombreUsuario;
    private javax.swing.JTextField cajaReseña;
    private javax.swing.JComboBox comboRestaurant;
    private javax.swing.JComboBox comboRestaurantEval;
    private javax.swing.JList comentarios;
    private datechooser.beans.DateChooserCombo dateSorteo;
    private javax.swing.JLabel descSorteoInfo;
    private javax.swing.JLabel direRstInfo;
    private javax.swing.JButton estrella1;
    private javax.swing.JButton estrella2;
    private javax.swing.JButton estrella3;
    private javax.swing.JButton estrella4;
    private javax.swing.JButton estrella5;
    private javax.swing.JCheckBox evalAnonima;
    private javax.swing.JLabel evalUsuarioInfo;
    private javax.swing.JLabel fechaSorteoInfo;
    private javax.swing.JLabel horarioInfo;
    private javax.swing.JButton infoEstrella1;
    private javax.swing.JButton infoEstrella2;
    private javax.swing.JButton infoEstrella3;
    private javax.swing.JButton infoEstrella4;
    private javax.swing.JButton infoEstrella5;
    private javax.swing.JLabel infoEvaluacionRestaurant;
    private javax.swing.JPanel infoSorteo;
    private javax.swing.JLabel infoSorteoRst;
    private javax.swing.JPanel informacion;
    private javax.swing.JPanel informacionEval;
    private javax.swing.JPanel informacionRestaurant;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel lblCantGanadores;
    private javax.swing.JList listaEvalInfo;
    private javax.swing.JList listaGanadores;
    private javax.swing.JList listaRestInfo;
    private javax.swing.JList listaSorteoInfo;
    private javax.swing.JList listaSorteos;
    private javax.swing.JLabel nombreRstInfo;
    private javax.swing.JPanel panelEvaluacion;
    private javax.swing.JPanel panelInformacion;
    private javax.swing.JPanel panelRealizarSorteo;
    private javax.swing.JPanel panelRestaurant;
    private javax.swing.JPanel panelSorteo;
    private javax.swing.JPanel paneles;
    private javax.swing.JLabel puntuacionInfo;
    private javax.swing.JLabel reseñaInfo;
    private javax.swing.JComboBox tipoComida;
    private javax.swing.JLabel tipoComidaInfo;
    private javax.swing.JButton ventHorarios;
    // End of variables declaration//GEN-END:variables
}
