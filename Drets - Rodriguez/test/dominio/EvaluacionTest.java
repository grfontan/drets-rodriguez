
package dominio;

import org.junit.Test;
import static org.junit.Assert.*;

public class EvaluacionTest {
    
    
    @Test
    public void esEstrellaValida1(){
        Evaluacion e = new Evaluacion();
        boolean es = e.estrellasValidas(1);
        assertEquals(es, true);
        
    }
    
    @Test
    public void esEstrellaValida2(){
        Evaluacion e = new Evaluacion();
        boolean es = e.estrellasValidas(3);
        assertEquals(es, true);
    }
    
    @Test
    public void esEstrellaValida3(){
        Evaluacion e = new Evaluacion();
        boolean es = e.estrellasValidas(5);
        assertEquals(es, true);
    }
    
    @Test
    public void noEsEstrellaValida1(){
        Evaluacion e = new Evaluacion();
        e.setEstrellas(0);
        boolean es = e.estrellasValidas(e.getEstrellas());
        assertEquals(es, false);
    }
    
    @Test
    public void noEsEstrellaValida2(){
        Evaluacion e = new Evaluacion();
        boolean es = e.estrellasValidas(6);
        assertEquals(es, false);
    }
    
    @Test
    public void testToStringEvaluacionAnonima(){
        //Creo una evaluacion con todos sus datos
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setEstrellas(3);
        evaluacion.setEsAnonima(true);
        evaluacion.setReseña("Descipcion de la evaluacion");
        evaluacion.setRestaurant(new Restaurant("La Pasiva"));
        evaluacion.setUsuario(new Usuario("Anónima", "Anónima"));
        
        String toString = evaluacion.toString();
        
        assertEquals(toString.equals("Anónima: Descipcion de la evaluacion"), true);
    }
    
    @Test
    public void testToStringEvaluacionNoAnonima(){
        //Creo una evaluacion con todos sus datos
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setEstrellas(3);
        evaluacion.setEsAnonima(false);
        evaluacion.setReseña("Descipcion de la evaluacion");
        evaluacion.setRestaurant(new Restaurant("La Pasiva"));
        evaluacion.setUsuario(new Usuario("Pedro", "pedro@gmail.com"));
        
        String toString = evaluacion.toString();
        
        assertEquals(toString.equals("pedro@gmail.com: Descipcion de la evaluacion"), true);
    }
    
    @Test
    public void dosEvaluacionesIguales(){
        Usuario usuario = new Usuario("Pedro", "pedro@gmail.com");
        Restaurant restaurant = new Restaurant("La Pasiva");
        
        //Creo una evaluacion con todos sus datos
        Evaluacion evaluacion1 = new Evaluacion();
        evaluacion1.setEstrellas(3);
        evaluacion1.setEsAnonima(false);
        evaluacion1.setReseña("Descipcion de la evaluacion");
        evaluacion1.setRestaurant(restaurant);
        evaluacion1.setUsuario(usuario);
        
        //Creo otra evaluacion con todos sus datos
        Evaluacion evaluacion2 = new Evaluacion(3, "Descipcion de la evaluacion",usuario,false);
       
        assertEquals(evaluacion1.equals(evaluacion2), true);
    }
    
    @Test
    public void dosEvaluacionesDistintas(){
        //Creo una evaluacion con todos sus datos
        Evaluacion evaluacion1 = new Evaluacion();
        evaluacion1.setEstrellas(3);
        evaluacion1.setEsAnonima(false);
        evaluacion1.setReseña("Descipcion de la evaluacion para La Pasiva");
        evaluacion1.setRestaurant(new Restaurant("La Pasiva"));
        evaluacion1.setUsuario(new Usuario("Pedro", "pedro@gmail.com"));
        
        //Creo otra evaluacion con todos sus datos
        Evaluacion evaluacion2 = new Evaluacion();
        evaluacion2.setEstrellas(4);
        evaluacion2.setEsAnonima(false);
        evaluacion2.setReseña("Descipcion de la evaluacion para Burger King");
        evaluacion2.setRestaurant(new Restaurant("Burger King"));
        evaluacion2.setUsuario(new Usuario("Jose", "jose@gmail.com"));
        
        
        assertEquals(evaluacion1.equals(evaluacion2), false);
    }
    
    @Test
    public void diferenteUsuario(){
        
        Evaluacion evaluacion1 = new Evaluacion();
        evaluacion1.setEstrellas(3);
        evaluacion1.setEsAnonima(false);
        evaluacion1.setReseña("Descipcion de la evaluacion");
        evaluacion1.setRestaurant(new Restaurant("La Pasiva"));
        evaluacion1.setUsuario(new Usuario("Pedro", "pedro@gmail.com"));
        
        Evaluacion evaluacion2 = new Evaluacion(3, "Descipcion de la evaluacion",
                                new Usuario("Pepe", "pepe@gmail.com"),false);
        
        assertEquals(evaluacion1.equals(evaluacion2),false);
    }
    
    @Test
    public void diferenteReseñas(){
        Usuario usuario = new Usuario("Pepe", "pepe@gmail.com");
        
        Evaluacion evaluacion1 = new Evaluacion();
        evaluacion1.setEstrellas(3);
        evaluacion1.setEsAnonima(false);
        evaluacion1.setReseña("Descipcion de la evaluacion laPasiva");
        evaluacion1.setRestaurant(new Restaurant("La Pasiva"));
        evaluacion1.setUsuario(new Usuario("Pedro", "pedro@gmail.com"));
        
        Evaluacion evaluacion2 = new Evaluacion(3, "Descipcion de la evaluacion",usuario,false);
        
        assertEquals(evaluacion1.equals(evaluacion2),false);
    }
    
    
    
    @Test
    public void sonObjetosDistintos(){
        
        Evaluacion evaluacion = new Evaluacion();
        Sorteo sorteo = new Sorteo();
        
        assertEquals(evaluacion.equals(sorteo),false);
    }
    
    @Test
    public void comparoConNull(){
        
        Evaluacion evaluacion = new Evaluacion();
        
        assertEquals(evaluacion.equals(null),false);
    }
}
