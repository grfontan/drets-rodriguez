package dominio;

import org.junit.Test;
import static org.junit.Assert.*;


public class UsuarioTest {
    
    @Test
    public void validarMailSinArrobas(){
        Usuario u = new Usuario();
        String unMail = "unMailhotmail.com";
        boolean resultado = u.esMailValido(unMail);
        assertEquals(resultado,false);
    }
    
    @Test
    public void validarMailConUnArrobas(){
        Usuario u = new Usuario();
        String unMail = "unMail@hotmail.com";
        boolean resultado = u.esMailValido(unMail);
        assertEquals(resultado,true);
    }
    
    @Test
    public void validarMailConMasArrobas(){
        Usuario u = new Usuario();
        String unMail = "un@Mail@hotmail@.com";
        boolean resultado = u.esMailValido(unMail);
        assertEquals(resultado,false);
    }
    
    @Test
    public void sonIgualesDosUsuarios(){
        Usuario u1 = new Usuario("Juan Drets", "juanDrets@hotmail.com");
        Usuario u2 = new Usuario("Juan Drets", "juanDrets@hotmail.com");
        
        boolean iguales = u1.equals(u2);
        assertEquals(iguales, true);
    }
    
    @Test
    public void noSonIgualesDosUsuarios(){
        Usuario u1 = new Usuario("Juan Drets", "juanDrets@hotmail.com");
        Usuario u2 = new Usuario("Guillermo Rodriguez", "guillermoro@hotmail.com");
        
        boolean iguales = u1.equals(u2);
        assertEquals(iguales, false);
    }
    
    @Test
    public void testToString(){
        //Creo un nuevo usuario con todos sus datos
        Usuario usuario = new Usuario();
        usuario.setNombre("Pedro Gonzalez");
        usuario.setMail("pedroGon@gmail.com");
        
        String toString = usuario.toString();
        
        assertEquals(toString.equals("pedroGon@gmail.com"), true);
    }
    
    @Test
    public void cambioElNombre(){
        Usuario u1 = new Usuario("Juan Drets", "juanDrets@hotmail.com");
        u1.setNombre("Guillermo Rodriguez");
        
        assertEquals(u1.getNombre().equals("Guillermo Rodriguez"), true);
    }
    
    @Test
    public void cambioElMail(){
        Usuario u1 = new Usuario("Juan Drets", "juanDrets@hotmail.com");
        u1.setMail("jdrets@gmail.com");
        
        assertEquals(u1.getMail().equals("jdrets@gmail.com"), true);
    }
    
    @Test
    public void sonObjetosDistintos(){
        
        Usuario usuario = new Usuario();
        Sorteo sorteo = new Sorteo();
        
        assertEquals(usuario.equals(sorteo),false);
    }
    
    @Test
    public void comparoConNull(){
        
        Usuario usuario = new Usuario();
        
        assertEquals(usuario.equals(null),false);
    }
}
