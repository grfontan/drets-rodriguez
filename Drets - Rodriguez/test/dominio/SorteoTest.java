package dominio;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class SorteoTest {
    
    @Test
    public void sorteoUsuarioUnico(){
        //Realiza el sorteo con un solo concursante y un solo ganador
        //El ganador debe ser ese usuario
        
        
        Sorteo elSorteo = new Sorteo();
        elSorteo.setNumeroGanadores(1);
        ArrayList<Usuario> listaUsuario = new ArrayList<>();
        Usuario unicoUsuario = new Usuario("Pedro","pedro@");
        listaUsuario.add(unicoUsuario);
        elSorteo.setUsuario(listaUsuario);
        
        elSorteo.realizarSorteo();
        boolean realizoSorteo = (elSorteo.getGanadores().size() == 1)
                               && elSorteo.getGanadores().get(0).equals(unicoUsuario);
        
        assertEquals(realizoSorteo,true);
    }
    
    @Test
    public void sorteoDosUsuario(){
        //Realiza el sorteo con dos concursante y dos ganadores
        //El ganador deben ser esos usuarios
        
        
        Sorteo elSorteo = new Sorteo();
        elSorteo.setNumeroGanadores(2);
        ArrayList<Usuario> listaUsuario = new ArrayList<>();
        Usuario unUsuario = new Usuario("Pedro","pedro@");
        listaUsuario.add(unUsuario);
        
        Usuario dosUsuario = new Usuario("Jose","jose@");
        listaUsuario.add(dosUsuario);
        
        elSorteo.setUsuario(listaUsuario);
        
        elSorteo.realizarSorteo();
        boolean realizoSorteo = (elSorteo.getGanadores().size() == 2)
                               && !elSorteo.getGanadores().get(0).equals(elSorteo.getGanadores().get(1));
        
        assertEquals(realizoSorteo,true);
    }
    
    @Test
    public void sorteoUnGanador(){
        //Realiza el sorteo con dos concursante y un ganadores
        //El ganador deben ser esos usuarios
        
        
        Sorteo elSorteo = new Sorteo();
        elSorteo.setNumeroGanadores(1);
        ArrayList<Usuario> listaUsuario = new ArrayList<>();
        Usuario unUsuario = new Usuario("Pedro","pedro@");
        listaUsuario.add(unUsuario);
        
        Usuario dosUsuario = new Usuario("Jose","jose@");
        listaUsuario.add(dosUsuario);
        
        elSorteo.setUsuario(listaUsuario);
        
        elSorteo.realizarSorteo();
        boolean realizoSorteo = (elSorteo.getGanadores().size() == 1)
                               && (elSorteo.getGanadores().get(0).equals(unUsuario)
                                    || elSorteo.getGanadores().get(0).equals(dosUsuario));
        
        assertEquals(realizoSorteo,true);
    }
    
    
    @Test
    public void sorteoSinGanador(){
        //Realiza el sorteo con ningun concursante y un premio
        //no se debe realizar el sorteo
        
        Sorteo elSorteo = new Sorteo();
        elSorteo.setNumeroGanadores(1);
        
        elSorteo.realizarSorteo();
        boolean realizoSorteo = (elSorteo.getGanadores().isEmpty());
        
        assertEquals(realizoSorteo,true);
    }
    
    @Test
    public void modificoFecha(){
        Sorteo sorteo = new Sorteo();
        sorteo.setFecha("24-12-2016");
        
        assertEquals(sorteo.getFecha().equals("24-12-2016"), true);
    }
    
    @Test
    public void modificoDescripcion(){
        Sorteo sorteo = new Sorteo();
        sorteo.setDescripcion("Modifico la descripcion del sorteo");
        
        assertEquals(sorteo.getDescripcion().equals("Modifico la descripcion del sorteo"), true);
    }
    
    @Test
    public void testToString(){
        //Creo un nuevo sorteo con todos sus datos
        Sorteo sorteo = new Sorteo();
        sorteo.setRestaurant(new Restaurant("La Pasiva"));
        sorteo.setDescripcion("Todos los premios");
        sorteo.setFecha("24-12-2016");
        sorteo.setGanadores(null);
        sorteo.setUsuario(null);
        
        String toString = sorteo.toString();
        
        assertEquals(toString.equals("La Pasiva"), true);
    }
    
   @Test
   public void sonSorteosIguales(){
       Restaurant restaurant = new Restaurant("La Pasiva");
       
       //Creo un sorteo con todos sus datos
       Sorteo sorteo1 = new Sorteo();
       sorteo1.setRestaurant(restaurant);
       sorteo1.setUsuario(null);
       sorteo1.setDescripcion("Todos los premios");
       sorteo1.setFecha("24-12-2016");
       sorteo1.setNumeroGanadores(3);
       sorteo1.setGanadores(null);
       
       //Creo otro sorteo con todos sus datos
       Sorteo sorteo2 = new Sorteo();
       sorteo2.setRestaurant(restaurant);
       sorteo2.setUsuario(null);
       sorteo2.setDescripcion("Todos los premios");
       sorteo2.setFecha("24-12-2016");
       sorteo2.setNumeroGanadores(3);
       sorteo2.setGanadores(null);
       
       assertEquals(sorteo2.equals(sorteo1), true);
   }
   
   @Test
   public void noSonSorteosIguales(){
       //Creo un sorteo con todos sus datos
       Sorteo sorteo1 = new Sorteo();
       sorteo1.setRestaurant(new Restaurant("La Pasiva"));
       sorteo1.setUsuario(null);
       sorteo1.setDescripcion("Todos los premios");
       sorteo1.setFecha("24-12-2016");
       sorteo1.setNumeroGanadores(3);
       sorteo1.setGanadores(null);
       
       //Creo otro sorteo con todos sus datos
       Sorteo sorteo2 = new Sorteo();
       sorteo2.setRestaurant(new Restaurant("Ponteveccio"));
       sorteo2.setUsuario(null);
       sorteo2.setDescripcion("Descripcion de los premios");
       sorteo2.setFecha("24-12-2016");
       sorteo2.setNumeroGanadores(3);
       sorteo2.setGanadores(null);
       
       assertEquals(sorteo2.equals(sorteo1), false);
   }
   
   @Test
    public void sonObjetosDistintos(){
        
        Sorteo sorteo = new Sorteo();
        Evaluacion evaluacion = new Evaluacion();
        
        assertEquals(sorteo.equals(evaluacion),false);
    }
    
    @Test
    public void comparoConNull(){
        
        Sorteo sorteo = new Sorteo();
        
        assertEquals(sorteo.equals(null),false);
    }
}
