package dominio;

import org.junit.Test;
import static org.junit.Assert.*;

public class RestaurantTest {
    
   @Test
   public void esDatoVacio(){
       Restaurant restaurantAux = new Restaurant();
       boolean dato = restaurantAux.esDatoVacio("  ");
       
       assertEquals(dato,true);
   }
   
   @Test
   public void noEsDatoVacio(){
       Restaurant restaurantAux = new Restaurant();
       boolean dato = restaurantAux.esDatoVacio("unRestaurant");
       
       assertEquals(dato,false);
   }
   
   @Test
   public void aumentoPuntacion(){
       Restaurant restaurantAux = new Restaurant();
       restaurantAux.agregarPuntacion(3);
       
       assertEquals(restaurantAux.getPuntacion(), 3);
   }
   
   @Test
   public void aumentoMasPuntacion(){
       Restaurant restaurantAux = new Restaurant();
       restaurantAux.agregarPuntacion(3);
       restaurantAux.agregarPuntacion(4);
       restaurantAux.agregarPuntacion(4);
       
       assertEquals(restaurantAux.getPuntacion(), 3);
   }
   
   @Test
   public void agregoTotalEstrellas(){
       Restaurant restaurant = new Restaurant();
       restaurant.setTotalEstrellas(5);
       
       assertEquals(true, restaurant.getTotalEstrellas() == 5);
   }
   
   @Test
   public void agregoCantEvaluaciones(){
       Restaurant restaurant = new Restaurant();
       restaurant.setCantEvaluaciones();
       
       assertEquals(true, restaurant.getCantEvaluaciones() == 1);
   }
   
   @Test
   public void agregoTipoComida(){
       Restaurant restaurant = new Restaurant();
       restaurant.setTipoComida("Pizza");
       
       assertEquals(true, restaurant.getTipoComida().equals("Pizza"));
   }
   
   @Test
   public void agregoHorario(){
       Restaurant restaurant = new Restaurant();
       restaurant.setHorario("Lunes 8:00 - 00:00");
       
       assertEquals(true, restaurant.getHorario().equals("Lunes 8:00 - 00:00"));
   }
   
   @Test
   public void agregoDireccion(){
       Restaurant restaurant = new Restaurant();
       restaurant.setDireccion("Rivera");
       
       assertEquals(true, restaurant.getDireccion().equals("Rivera"));
   }
   
   @Test
   public void sonIguales(){
       Restaurant restaurantAux = new Restaurant();
       restaurantAux.setNombre("La Pasiva");
       
       Restaurant restaurantIgual = new Restaurant("La Pasiva", "Rivera y Soca", "13 - 24", "pizza");
       
       boolean isEqual = restaurantIgual.equals(restaurantAux);
       
       assertEquals(isEqual, true);
   }
   
   @Test
   public void noSonIguales(){
       Restaurant restaurantAux = new Restaurant();
       restaurantAux.setNombre("La Pasiva");
       
       Restaurant restaurantIgual = new Restaurant();
       restaurantIgual.setNombre("Mc Donalds");
       
       boolean isEqual = restaurantIgual.equals(restaurantAux);
       
       assertEquals(isEqual, false);
   }
   
   @Test
   public void testToString(){
       //Cargo un nuevo restaurant con todos los datos
       Restaurant restaurantAux = new Restaurant();
       restaurantAux.setNombre("La Pasiva");
       restaurantAux.setDireccion("Rivera y Soca");
       restaurantAux.setHorario("Lunes a Domingo de 8 a 00");
       restaurantAux.setTipoComida("Pizzas");
       restaurantAux.setTotalEstrellas(0);
       restaurantAux.setPuntacion();
       
       String toString = restaurantAux.toString();
       
       assertEquals(toString.equals("La Pasiva"), true);
   }
   
   @Test
    public void sonObjetosDistintos(){
        
        Restaurant restaurant = new Restaurant();
        Sorteo sorteo = new Sorteo();
        
        assertEquals(restaurant.equals(sorteo),false);
    }
    
    @Test
    public void comparoConNull(){
        
        Restaurant restaurant = new Restaurant();
        
        assertEquals(restaurant.equals(null),false);
    }
}
