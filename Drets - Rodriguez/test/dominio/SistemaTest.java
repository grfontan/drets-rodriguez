package dominio;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {
    
    @Test
    public void noParticipanDelSorteo(){
        Sistema sistema = new Sistema();
        Sorteo sorteo = new Sorteo();
        Restaurant restaurant = new Restaurant();
        restaurant.setNombre("La Pasiva");
        
        //Creo evaluacion anonima para ese restaurant
        //No participa del sorteo
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setEsAnonima(true);
        evalAux1.setUsuario(new Usuario("Anónimo", "Anónimo"));
        evalAux1.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo una evaluacion no anonima para otro restaurant
        //No participa del sorteo
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setEsAnonima(false);
        evalAux2.setUsuario(new Usuario("Pedro", "Pedro@"));
        evalAux2.setRestaurant(new Restaurant("Mc Donalds"));
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Cargo los usuarios a la lista de usuarios del sorteo
        sistema.usuariosParticipantes(sorteo, restaurant);
        
        //Ningun concursante en la lista de usuarios del sorteo
        assertEquals(sorteo.getUsuario().isEmpty(), true);
    }
    
    @Test
    public void participaUnoEnElSorteo(){
        Sistema sistema = new Sistema();
        Sorteo sorteo = new Sorteo();
        Restaurant restaurant = new Restaurant();
        Usuario usuario = new Usuario("Pedro", "Pedro@");
        restaurant.setNombre("La Pasiva");
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setEsAnonima(false);
        evalAux1.setUsuario(usuario);
        evalAux1.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo una evaluacion no anonima para otro restaurant
        //No participa del sorteo
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setEsAnonima(false);
        evalAux2.setUsuario(usuario);
        evalAux2.setRestaurant(new Restaurant("Mc Donalds"));
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo evaluacion anonima para ese restaurant
        //No participa del sorteo
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setEsAnonima(true);
        evalAux3.setUsuario(new Usuario("Anónimo", "Anónimo"));
        evalAux3.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Cargo los usuarios a la lista de usuarios del sorteo
        sistema.usuariosParticipantes(sorteo, restaurant);
        
        //Solo un concursante en la lista de usuarios del sorteo
        assertEquals(sorteo.getUsuario().size() == 1, true);
    }
    
    @Test
    public void participanMuchosDelSorteo(){
        Sistema sistema = new Sistema();
        Sorteo sorteo = new Sorteo();
        Restaurant restaurant = new Restaurant();
        restaurant.setNombre("La Pasiva");
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setEsAnonima(false);
        evalAux1.setUsuario(new Usuario("Pedro", "Pedro@"));
        evalAux1.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setEsAnonima(false);
        evalAux2.setUsuario(new Usuario("Jose", "Jose@"));
        evalAux2.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setEsAnonima(false);
        evalAux3.setUsuario(new Usuario("Jose", "Jose@"));
        evalAux3.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Cargo los usuarios a la lista de usuarios del sorteo
        sistema.usuariosParticipantes(sorteo, restaurant);
        
        //Tres concursante en la lista de usuarios del sorteo
        assertEquals(sorteo.getUsuario().size() == 3, true);
        
    }
    
    @Test
    public void participaUnoDelSorteo(){
        Sistema sistema = new Sistema();
        Sorteo sorteo = new Sorteo();
        Restaurant restaurant = new Restaurant();
        restaurant.setNombre("La Pasiva");
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setEsAnonima(false);
        evalAux1.setUsuario(new Usuario("Pedro", "Pedro@"));
        evalAux1.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setEsAnonima(false);
        evalAux2.setUsuario(new Usuario("Jose", "Jose@"));
        evalAux2.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo evaluacion no anonima para ese restaurant
        //participa del sorteo
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setEsAnonima(false);
        evalAux3.setUsuario(new Usuario("Jose", "Jose@"));
        evalAux3.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Cargo los usuarios a la lista de usuarios del sorteo
        sistema.usuariosParticipantes(sorteo, restaurant);
        
        //Tres concursante en la lista de usuarios del sorteo
        assertEquals(sorteo.getUsuario().size() == 3, true);
        
    }
    
    @Test
    public void sinEvaluacionParaElRestaurant(){
        Sistema sistema = new Sistema();
        Restaurant restaurant = new Restaurant("La Pasiva");
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setRestaurant(new Restaurant("Mc Donalds"));
        sistema.getListaEvaluacion().add(evalAux1);
        
         //Creo otra evaluacion para otro restaurant
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setRestaurant(new Restaurant("Pizza Hut"));
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo otra evaluacion para otro restaurant
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setRestaurant(new Restaurant("Burger King"));
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Creo otra evaluacion para otro restaurant
        Evaluacion evalAux4 = new Evaluacion();
        evalAux4.setRestaurant(new Restaurant("Chivitos Marcos"));
        sistema.getListaEvaluacion().add(evalAux4);
        
        ArrayList<Evaluacion> listaEvalPorRestaurant = sistema.evaluacionPorRestaurant(restaurant);
        
        //El este caso el largo debe ser 0
        assertEquals(listaEvalPorRestaurant.isEmpty(), true);
        
    }
    
    @Test
    public void unaEvaluacionParaEseRestaurant(){
        Sistema sistema = new Sistema();
        Restaurant restaurant = new Restaurant("La Pasiva");
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setRestaurant(new Restaurant("Mc Donalds"));
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo evaluacion para ese restaurant
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setRestaurant(new Restaurant("Burger King"));
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux4 = new Evaluacion();
        evalAux4.setRestaurant(new Restaurant("Pizza And Love"));
        sistema.getListaEvaluacion().add(evalAux4);
        
        ArrayList<Evaluacion> listaEvalPorRestaurant = sistema.evaluacionPorRestaurant(restaurant);
        
        //En este caso el largo debe ser 1
        assertEquals(listaEvalPorRestaurant.size() == 1, true);
    }
    
    @Test
    public void variasEvaluacionesParaElRestaurant(){
        Sistema sistema = new Sistema();
        Restaurant restaurant = new Restaurant("La Pasiva");
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux1 = new Evaluacion();
        evalAux1.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux1);
        
        //Creo evaluacion para ese restaurant
        Evaluacion evalAux2 = new Evaluacion();
        evalAux2.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux2);
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux3 = new Evaluacion();
        evalAux3.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux3);
        
        //Creo evaluacion para otro restaurant
        Evaluacion evalAux4 = new Evaluacion();
        evalAux4.setRestaurant(restaurant);
        sistema.getListaEvaluacion().add(evalAux4);
        
        ArrayList<Evaluacion> listaEvalPorRestaurant = sistema.evaluacionPorRestaurant(restaurant);
        
        //En este ejemplo el largo debe ser 4
        assertEquals(listaEvalPorRestaurant.size() == 4, true);
    }
    
    @Test
    public void seteoListaUsuario(){
        ArrayList<Usuario> lista = new ArrayList<>();
        Sistema sistema = new Sistema();
        sistema.setListaUsuarios(lista);
        
        assertEquals(true, sistema.getListaUsuarios().equals(lista));
        
    }
    
    @Test
    public void seteoListaEvaluacion(){
        ArrayList<Evaluacion> lista = new ArrayList<>();
        Sistema sistema = new Sistema();
        sistema.setListaEvaluacion(lista);
        
        assertEquals(true, sistema.getListaEvaluacion().equals(lista));
        
    }
    
    @Test
    public void seteoListaSorteo(){
        ArrayList<Sorteo> lista = new ArrayList<>();
        Sistema sistema = new Sistema();
        sistema.setListaSorteo(lista);
        
        assertEquals(true, sistema.getListaSorteo().equals(lista));
        
    }
    
    @Test
    public void seteoListaRestaurant(){
        ArrayList<Restaurant> lista = new ArrayList<>();
        Sistema sistema = new Sistema();
        sistema.setListaRestaurant(lista);
        
        assertEquals(true, sistema.getListaRestaurant().equals(lista));
        
    }
}
